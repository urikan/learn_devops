import requests
import json
from PIL import Image

# -------Case 2 solution-------
class Photo:
    def __init__(self,id,title, url,thumbnailUrl):
        self.id=id
        self.title=title
        self.url=url
        self.thumbnailUrl=thumbnailUrl
    def __str__(self):
        return f'Photo id={self.id} title={self.title} url={self.url} thumbnail={self.thumbnailUrl}'
    def __repr__(self):
        return f'Photo({self.id},{self.title},{self.url},{self.thumbnailUrl})'
    def __eq__(self, other):
        return self.id==other.id
    def __gt__(self,other):
        return self.id>other.id
    def __lt__(self, other):
        return self.id<other.id

photo1=Photo(1,'bu',"https://via.placeholder.com/600/92c952","https://via.placeholder.com/150/92c952")
photo2=Photo(2,'ba',"https://via.placeholder.com/600/771796","https://via.placeholder.com/150/771796")
photo3=Photo(2,'ba',"https://via.placeholder.com/600/771796","https://via.placeholder.com/150/771796")

print(photo1)
print(photo1.__repr__())
print(photo1==photo2)
print(photo3==photo2)
print(photo1<photo2)
print(photo3>photo2)

img = Image.open(requests.get(photo1.url, stream=True).raw)
img.show()

# -------Case 3 solution-------
resp = requests.get('https://jsonplaceholder.typicode.com/photos/1')
photo_data = json.loads(resp.content)
photo4=Photo(photo_data['id'],photo_data['title'],photo_data['url'],photo_data['thumbnailUrl'])
print(photo4)


# -------Case 4 solution-------
class QuickPhoto:
    def __init__(self,data_dict):
        self.__dict__=data_dict
    def __str__(self):
        out_str='QuickPhoto'
        for k,v in self.__dict__.items():
            out_str=out_str+f' {k}={v}'
        return out_str

quickPhoto=QuickPhoto(photo_data)
print(quickPhoto)


# -------Case 5 solution-------
baseUrl='https://jsonplaceholder.typicode.com/photos/'
photosList=[]
for id in range(1,11):
    resp = requests.get(f'{baseUrl}{id}')
    photo_data = json.loads(resp.content)
    photosList.append(QuickPhoto(photo_data))

# -------Case 6 solution-------
class Albom:
    photosList = []
    def __init__(self,serverUrl,picturesAmount):
        for id in range(1, picturesAmount+1):
            resp = requests.get(f'{serverUrl}{id}')
            photo_data = json.loads(resp.content)
            self.photosList.append(QuickPhoto(photo_data))

albom=Albom(baseUrl,20)

# -------Case 7 solution-------
id=input('please provide id for required photo: ')
resp = requests.get(f'{baseUrl}{id}')
respose_code=resp.status_code
if respose_code==200:
    photo_data = json.loads(resp.content)
    print(QuickPhoto(photo_data))
else:
    print(f'no photo was found for id {id}')

# -------Case 8 solution-------
class QuickPhotoRestClient:
    def __init__(self,url):
        self.url=url
    def get(self,id):
        requests.get(f'{self.url}{id}')
    def post(self,quick_photo):
        resp = requests.post(self.url, data = json.dumps(quick_photo.__dict__))
        return json.loads(resp.content)['id']
    def put(self,id,quick_photo):
        requests.put(f'{self.url}{id}',data = json.dumps(quick_photo.__dict__))
    def delete(self,id):
        requests.delete(f'{self.url}{id}')

qpRestClient=QuickPhotoRestClient(baseUrl)
id=qpRestClient.post(quickPhoto)
qpRestClient.get(id)
qpRestClient.put(id,quickPhoto)
qpRestClient.delete(id)