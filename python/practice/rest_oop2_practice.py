import requests
import json

# -------Case 2 solution-------
class MyUser:
    def __init__(self,id,name,username,email):
        self.id=id
        self.name=name
        self.username=username
        self.email=email
    def __str__(self):
        return f'MyUser id={self.id} name={self.name} username={self.username} email={self.email}'

# -------Case 3 solution-------
baseUrl='https://jsonplaceholder.typicode.com/users'

def findUserByName():
    name=input('please provide name of user: ')
    response = requests.get(baseUrl)
    users = json.loads(response.content)
    for user_dict in users:
        user=MyUser(user_dict['id'],user_dict['name'],user_dict['username'],user_dict['email'])
        if user.name==name:
            return user
    return 'user not found'

#findUserByName()

# -------Case 4 solution-------
class SpeedUser:
    def __init__(self,data_dict):
        self.__dict__=data_dict
    def __str__(self):
        out_str='SpeedUser'
        for k,v in self.__dict__.items():
            out_str=out_str+f' {k}={v}'
        return out_str

# -------Case 5 solution-------
def findUserByLatitude():
    input_lat = float(input('please provide latitude of user: '))
    response = requests.get(baseUrl)
    users_json = json.loads(response.content)
    users=[]
    latitude_deltas=[]
    for user_dict in users_json:
        user=SpeedUser(user_dict)
        latitude=float(user.address['geo']['lat'])
        if latitude==input_lat:
            return user
        latitude_deltas.append(abs(latitude-input_lat))
        users.append(user)
    least_delta=min(latitude_deltas)
    least_index=latitude_deltas.index(least_delta)
    return users[least_index]

#findUserByLatitude()

# -------Case 6 solution-------
baseUrl='https://randomuser.me/api/'

users_amount=int(input('please provide amount of users (less than 100): '))
for i in range(users_amount):
    while True:
        response = requests.get(baseUrl)
        if response.status_code==200:
            break
    user_json=json.loads(response.content)
    name_json=user_json['results'][0]['name']
    print(f"User name: {name_json['title']} {name_json['first']} {name_json['last']}")
