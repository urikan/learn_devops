#-------Question 1 solution-------
import random

def hide_treasure(path):
    with open(path,'w') as file:
        for num in range(10):
            write_group(num, file)
        file.write('TREASURE')
        for num in range(9,-1,-1):
            write_group(num, file)

def write_group(num,file):
    group_length=random.randint(1,20)
    for i in range(group_length):
        file.write(str(num))

def get_seek_instruction():
    while True:
        user_inp = input('where you want to move? (e.g. 5 places forward: +5): ')
        dir=user_inp[0]
        if dir not in '-+':
            print('illegal input')
            continue
        step_str=user_inp[1:]
        if not step_str.isdigit():
            print('illegal input')
            continue
        if dir=='-':
            return 0 - int(step_str)
        if dir=='+':
            return int(step_str)

def check_position(position,file):
    file.seek(position)
    found_item=file.read(1)
    file.seek(position)
    print(f'you hit {found_item}')
    return found_item in 'TREASURE'

def find_treasure(path):
    with open(path, 'r') as file:
        file.seek(0, 2)
        max_pos = file.tell() - 1
        min_pos = 0
        position = 0
        file.seek(0)
        found_treasure = False
        step_count = 0
        while not found_treasure:
            step = get_seek_instruction()
            position = position + step
            if position < min_pos:
                position = min_pos
            if position > max_pos:
                position = max_pos
            found_treasure = check_position(position, file)
            step_count = step_count + 1
    print('you found the treasure!!')
    print(f'took you {step_count} steps')
    return step_count

'''
covers the following cases:
1. 10 records and worse score
2. 10 records and better score
3. less than 10 records
4. existing player with same or worse score
5. existing player with better score
'''
def check_new_hiscore(steps,path):
    with open(path, 'r') as file:
        hiscore_dict={}
        for line in file:
            line_data=line.split()
            hiscore_dict[line_data[0]]=int(line_data[1])
    if len(hiscore_dict)==10:
        worst_score=max(hiscore_dict.values())
        if steps>=worst_score:
            return
    player_name=input('give your name for the wall of fame: ')
    if player_name in hiscore_dict.keys():
        if hiscore_dict[player_name]<=steps:
            print('such or better record for the player already exists')
            return
        print('updating record for the player')
    elif len(hiscore_dict)==10:
        worst_list=[]
        for name,score in hiscore_dict.items():
            if score==worst_score:
                worst_list.append(name)
        hiscore_dict.pop(worst_list[random.randrange(len(worst_list))])
    hiscore_dict[player_name]=steps
    save_new_hiscore(hiscore_dict, path)

def save_new_hiscore(hiscore_dict,path):
    scores=[]
    for score in hiscore_dict.values():
        if not score in scores:
            scores.append(score)
    scores.sort()
    with open(path, 'w') as file:
        for score in scores:
            for name in hiscore_dict.keys():
                if hiscore_dict[name]==score:
                    file.write(f'{name} {score}\n')
    print('new high score was recorded')

treasure_path='treasure.txt'
hiscore_path='hi_score.txt'
hide_treasure(treasure_path)
print('find the treasure!!')
steps=find_treasure(treasure_path)
check_new_hiscore(steps,hiscore_path)

#-------Question 2 solution-------
def GetFileSize(path):
    with open(path, 'r') as file:
        file.seek(0, 2)
        return file.tell()

#-------Question 3 solution-------
def GetAllSizes(paths):
    return [GetFileSize(path) for path in paths]

#-------Question 4 solution-------
def GetSumSize(paths):
    sum=0
    for size in GetAllSizes(paths):
        sum=sum+size
    return sum

#-------Question 5 solution-------
def GetWordsFromFile(path):
    words = set()
    with open(path, 'r') as file:
        for line in file:
            for word in line.split():
                words.add(word)
    return words

#-------Question 6 solution-------
def GetWordsCountFromFile(path):
    words={}
    with open(path, 'r') as file:
        for line in file:
            for word in line.split():
                if word in words:
                    words[word]=words[word]+1
                else:
                    words[word]=1
    return words

#-------Question 7 solution-------
def WriteReverse(path_in,path_out):
    with open(path_in, 'r') as file_in:
        file_in.seek(0,2)
        file_size=file_in.tell()
        position=file_size
        block_size=10
        first_block=True
        with open(path_out, 'w') as file_out:
            while position!=0:
                new_position=position-block_size
                if new_position<0:
                    new_position=0
                file_in.seek(new_position)
                chars_to_read=position-new_position
                text=file_in.read(chars_to_read)
                if not first_block:
                    if '\n' in text:
                        text=text[:chars_to_read-1]
                else:
                    first_block=False
                file_out.write(text[::-1])
                position=new_position

#-------Question 8 solution-------
def FindWord(word,path):
    words=GetWordsFromFile(path)
    return word in words

#-------Question 9 solution-------
def AddWordIfNotExist(word,path):
    if not FindWord(word,path):
        with open(path, 'a') as file:
            file.write(f' {word}')