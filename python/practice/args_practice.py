#-------Question 1 solution-------
def get_args_as_list(*args):
    var_list=[]
    if args!=None:
        for arg in args:
            var_list.append(arg)
    return var_list

#-------Question 2 solution-------
def get_args_as_dictionary(a,b,*args):
    var_list = None
    if args != None:
        var_list = []
        for arg in args:
            var_list.append(arg)
    return {'positional_arguments':[a,b],'*args':var_list}

#-------Question 3 solution-------
def get_tuple_from_list(arg_list):
    if type(arg_list)!=list:
        return None
    return tuple(arg_list)

#-------Question 4 solution-------
def counter_tuple(*args):
    count_dict={}
    for member in args:
        if member in count_dict:
            count_dict[member]=count_dict[member]+1
        else:
            count_dict[member]=1
    return count_dict

#-------Question 5 solution-------
def discovery(param):
    if type(param)==list:
        print('list can be modified in any way')
    elif type(param)==tuple:
        print('tuple cannot be modified in any way')
    elif type(param)==dict:
        print('dictionary can be modified in any way')
    elif type(param)==set:
        print('set can be modified. but duplicated values will be ignored!')
