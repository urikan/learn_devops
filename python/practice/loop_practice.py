# -------Question 1 solution-------
number=int(input("Please provide a number: "))
half_way=number//2
for divider in range(1,half_way+1):
    if number%divider==0:
        print(divider)

# -------Question 2 solution-------
user_num=int(input('please insert a number #1: '))
num_sum=0
length=0
while user_num>=0:
    num_sum=num_sum+user_num
    length=length+1
    user_num=int(input(f'please insert a number #{length+1} (avg={num_sum/length:.2f}. Sum={num_sum}): '))
print('Thank you. Goodbye.')

# -------Question 3 solution-------
words_dict={}
while True:
    word=input('Please type a word: ')
    word_upper=word.upper()
    if word_upper in words_dict.keys():
        word_count=words_dict[word_upper]
        if word_count<2:
            words_dict[word_upper]=word_count+1
        else:
            print(f'you have entered the word {word} three times. Good bye...')
            break
    else:
        words_dict.update({word_upper:1})
    print(words_dict)

# -------Question 4 solution-------
num_list1=[]
num_list2=[]
print('please load first list. use empty char to stop.')
while True:
    str_num=input(f'#{count}: ')
    if str_num=='':
        break
    num_list1.append(int(str_num))
    print(num_list1)

print('please load second list. use empty char to stop.')
while True:
    str_num=input(f'#{count}: ')
    if str_num=='':
        break
    num_list2.append(int(str_num))
    print(num_list2)

print(f'first list is {num_list1}')
print(f'second list is {num_list2}')
len1=len(num_list1)
len2=len(num_list2)
if len(num_list1)==0 or len(num_list2)==0:
    print('one of lists is empty')
    exit()

if len1>len2:
    len_common=len2
else:
    len_common=len1

score1=0
score2=0
for index in range(len_common):
    if num_list1[index]>num_list2[index]:
        score1+=1
    elif num_list1[index]<num_list2[index]:
        score2+=1

if score1>score2:
    print('first list is bigger')
elif score1 < score2:
    print('second list is bigger')
else:
    print('lists are even')