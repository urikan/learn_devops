#-------Question 8 solution-------
x=int(input('Please input first number: '))
y=int(input('Please input second number: '))

big=x
if y>big:
    big=y
print(f'The biggest number is {big}')

#-------Question 9 solution-------

x=int(input('Please input first number: '))
y=int(input('Please input second number: '))
z=int(input('Please input third number: '))

big=x
if y>big:
    big=y
if z>big:
    big=z
print(f'The biggest number is {big}')

#-------Question 10 solution-------
eq=input('Please type a sum equation: ')
eq_parts=eq.split()
if len(eq_parts)!=5 or eq_parts[3]!='=':
    print('bad equation')
    exit()
if eq_parts[1]!='+':
    print('only + operator is supported')
    exit()
num1=float(eq_parts[0])
num2=float(eq_parts[2])
res=float(eq_parts[4])
if num1+num2==res:
    print("TRUE")
else:
    print("FALSE")

#-------Question 11 solution-------
eq=input('Please type a sum equation: ')
eq_parts=eq.split()
op=eq_parts[1]
if len(eq_parts)!=5 or eq_parts[3]!='=':
    print('bad equation')
    exit()
if op not in ['+','-','*','/']:
    print('only +,-,*,/ operators are supported')
    exit()
num1=float(eq_parts[0])
num2=float(eq_parts[2])
res=float(eq_parts[4])
if op=='+':
    print(str(num1 + num2 == res).upper())
elif op=='-':
    print(str(num1 - num2 == res).upper())
elif op=='*':
    print(str(num1 * num2 == res).upper())
else:
    print(str(num1 / num2 == res).upper())