# -------Question 15 solution-------
def parseInput(inputString):
    inputData=inputString.split()
    firstName=inputData[0]
    lastName=inputData[1]
    id=int(inputData[2])
    return firstName,lastName,id

print('please provide first name, last name and id')
while True:
    try:
        userInput=input()
        firstName,lastName,id=parseInput(userInput)
        print(f'successfully received: firstName - {firstName}, lastName - {lastName}, id - {id}')
        break
    except IndexError:
        print('Error: not enough data provided. Please try again.')
    except ValueError:
        print('Error: illegal id. Please try again.')
    except Exception:
        print('Unexpected error. Please try again.')