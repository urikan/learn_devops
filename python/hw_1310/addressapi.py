class Address:
    def __init__(self,Country,City,Street):
        self.Country=Country
        self.City=City
        self.Street=Street
    def __str__(self):
        out_str='Address'
        for k,v in self.__dict__.items():
            out_str=out_str+f' {k}={v}'
        return out_str

class AddressParser:
    def getAddress(self,Country,City,Street):
        return Address(Country,City,Street)

def main():
    print('using Address')

if __name__ == "__main__":
    main()