import requests
import json

class WebRest:
    def getWebResult(self,serviceurl,id):
        fullurl=f'{serviceurl}{id}'
        response=requests.get(fullurl)
        if response.status_code==200:
            return WebResult(json.loads(response.content))
        return 'result: no data'

class WebResult:
    def __init__(self, data_dict):
        self.__dict__ = data_dict
    def __str__(self):
        out_str='Object:'
        for k,v in self.__dict__.items():
            out_str=out_str+f' {k}={v}'
        return out_str
