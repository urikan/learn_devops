# -------Question 10 solution-------
import random
class BankAccount:
    def __init__(self, accountNumber, firstName, lastName, balance):
        self.accountNumber = accountNumber
        self.firstName = firstName
        self.lastName = lastName
        self.__balance = balance

    def __repr__(self):
        return f'BankAccount({self.accountNumber},{self.firstName},{self.lastName},{self.__balance})'

    def __str__(self):
        return f'account={self.accountNumber} first name={self.firstName} last name={self.lastName} balance={self.__balance}'

    def __eq__(self, other):
        return self.accountNumber == other.accountNumber

    def __gt__(self, other):
        return self.__balance > other.__balance

    def __add__(self, other):
        if type(other).__name__=='BankAccount':
            accountFound=False
            while not accountFound:
                newAccountNumber=random.randint(1,100)
                if newAccountNumber!=self.accountNumber and newAccountNumber!=other.accountNumber:
                    accountFound=True
            return BankAccount(newAccountNumber,self.firstName+other.firstName,self.lastName+other.lastName,self.__balance+other.__balance)
        if type(other).__name__=='int':
            self.__balance=self.__balance+other

    def __ne__(self, other):
        return not self==other

    def __sub__(self, other):
        return self.__balance - other.__balance

    def __mul__(self, other):
        return self.__balance * other.__balance

    def getBalance(self,country='Israel'):
        if country=='Israel':
            return self.__balance
        elif country=='US':
            return self.__balance/3.7
        elif country == 'France':
            return self.__balance / 3.88