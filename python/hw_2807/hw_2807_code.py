#-------Question 8 solution-------
for num in range(200,1,-1):
    print(num)
    
#-------Question 9 solution-------
for num in range(1,100):
    if num%7==0:
        print(num)

#-------Question 10 solution-------
sum=0
while True:
    num=int(input('Please insert a number: '))
    if num<0:
        break
    sum=sum+num
print('sum of your numbers is {0}'.format(sum))

#-------Question 11 solution-------
target=int(input('Please insert number: '))
factorial=1
for num in range(target):
    factorial=factorial*(num+1)
print('factorial of {0} is {1}'.format(target,factorial))

#-------Question 12 solution-------
secret=[25,16,6,42,32]
guess_track=secret.copy()
tries=0
min=2
max=49
while True:
    num=int(input('Guess a number: '))
    if num<min or num>max:
        continue
    tries=tries+1
    if num in guess_track:
        print('you are lucky!!')
        guess_track.remove(num)
        if len(guess_track)==0:
            summary=f'you have guessed the lucky numbers in {tries}'
            break
    elif num in secret:
        summary='typed twice a lucky number'
        break
    if tries==19:
        print('used 19 tries. restarting the game.')
        guess_track=secret.copy()
        tries=0
print(summary)