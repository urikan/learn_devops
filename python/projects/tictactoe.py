import random

def print_board(board):
    "print_board:"
    "prints out the game board"
    "board: game board"
    "no return value"
    for row in board:
        print(f'|{row[0]}', end = '')
        for char in row[1:]:
            print(f',{char}', end = '')
        print('|')

def get_winner(board, player_tokens):
    "get_winner:"
    "checks board for game status"
    "board: game board"
    "player_tokens: player token"
    "returns game status: 0 - player0 wins, 1 - player1, 2 - draw, None - continue"
    size=len(board)
    check_list=[]
    
    for row in board:
        check_list.append(row)
     
    for index in range(size):
        column=[]
        for row in board:
            column.append(row[index])
        check_list.append(column)
        
    diagonal0=[]
    diagonal1=[]
    for index in range(size):
        diagonal0.append(board[index][index])
        diagonal1.append(board[size-1-index][index])
    check_list.append(diagonal0)
    check_list.append(diagonal1)
    
    for list in check_list:
        token=list_filler(list)
        if token in player_tokens:
            return player_tokens.index(token)
    
    if get_free_cells(board)==0:
        return 2
    return None
    
def list_filler(list):
    "list_filler:"
    "checks for chars in list (row, column or diagonal)"
    "list: inspected list"
    "returns list char, None for multiple"
    first_char=list[0]
    for char in list[1:]:
        if char!=first_char:
            return None
    return first_char

def human_player_play(board, active_player, player_token):
    "human_player_play:"
    "make a human player move in game"
    "board: game board"
    "active_player: active player index"
    "player_token: active player token"
    "no return value"
    row=None
    column=None
    while True:
        row=get_user_input(board, active_player, 'row')
        column=get_user_input(board, active_player, 'column')
        if cell_is_free(board, row, column):
            break
        print('cell is not empty')
    board[row][column]=player_token

def pc_player_play(board, active_player, player_token):
    "pc_player_play:"
    "make a pc player move in game"
    "board: game board"
    "active_player: active player index"
    "player_token: active player token"
    "no return value"
    size=len(board)
    free_cells=get_free_cells(board)
    chosen_cell=random.randint(0, free_cells - 1)
    cell=0
    cell_found=False
    for row in range(size):
        for column in range(size):
            if cell_is_free(board, row, column):
                if cell==chosen_cell:
                    cell_found=True
                    break
                cell=cell+1
        if cell_found:
            break
    board[row][column]=player_token
    print(f'player{active_player} move: row - {row}, column - {column}')
    input('press enter key to continue')

def get_free_cells(board):
    "get_free_cells:"
    "get amount of free cells on board"
    "board: game board"
    "returns amount of free cells"
    size=len(board)
    free_cells=0
    for row in range(size):
        for column in range(size):
            if cell_is_free(board, row, column):
                free_cells= free_cells + 1
    return free_cells
    
def get_user_input(board, active_player, dimension):
    "get_user_input:"
    "get user input for row or column"
    "board: game board"
    "active_player: active player index"
    "dimension: row or column"
    "returns number of row or column"
    size=len(board)
    while True:    
        user_input=input(f'player{active_player} {dimension}: ')
        if not user_input.isdigit():
            print('number is required')
            continue
        user_input_num=int(user_input)
        if user_input_num not in range(size):
            print(f'{dimension} not in range')
            continue
        return user_input_num
        
def cell_is_free(board, row, column):
    "cell_is_free:"
    "check if board cell is free"
    "board: game board"
    "row: row number"
    "column: column number"
    "returns True if cell is free"
    return board[row][column]=='_'

def get_players():
    "get_players:"
    "define player types"
    "returns player types"
    players=[]
    for index in range(2):
        while True:
            player=input(f'player{index} type (h - human, p - pc): ')
            if player in ['h','p']:
                players.append(player)
                break
            print('illegal player type')
    return players

def check_next_winner_move(board, active_player, player_tokens):
    "check_next_winner_move:"
    "display next winner move if such exists"
    "active_player: active player index"
    "player_tokens: player tokens"
    "no return value"
    size=len(board)
    winner_found=False
    for row in range(size):
        for column in range(size):
            if cell_is_free(board, row, column):
                board[row][column]=player_tokens[active_player]
                if active_player==get_winner(board, player_tokens):
                    print(f'marking cell at {row},{column} is a winner')    
                    winner_found=True
                board[row][column]='_'
                if winner_found:
                    return

def create_board():
    "create_board:"
    "define board size and create board"
    "returns board"
    while True:
        user_input=input('define board size: ')
        if user_input.isdigit():
            size=int(user_input)
            if size>0:
                break
        print('size must be positive integer')
    board=[]
    for r in range(size):
        row=[]
        for c in range(size):
            row.append('_')
        board.append(row)
    return board

#main code
print('welcome to tictactoe game')
board=create_board()
print_board(board)
players_config=get_players()
player_tokens=['X', 'O']
move=0
while True:
    active_player= move % 2
    check_next_winner_move(board, active_player, player_tokens)
    if players_config[active_player]== 'h':
        human_player_play(board, active_player, player_tokens[active_player])
    elif players_config[active_player]== 'p':
        pc_player_play(board, active_player, player_tokens[active_player])
    print_board(board)
    winner=get_winner(board, player_tokens)
    if winner!=None:
        break
    move=move+1
if winner==2:
    print('game over. it`s a draw.')
else:
    print(f'player{winner} is the winner')