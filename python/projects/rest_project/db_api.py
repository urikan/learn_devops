import psycopg2,json

class DbError(Exception):
    def __init__(self,msg):
        self.msg=msg

class PgSqlConnection:
    def __init__(self,logger):
        with open('server_conf.json') as json_file:
            conf = json.load(json_file)
        self.server=conf['db']['server']
        self.user=conf['db']['user']
        self.password=conf['db']['password']
        self.database=conf['db']['name']
        self.logger=logger

    def getUsers(self):
        with psycopg2.connect(host=self.server, database=self.database, user=self.user,
                              password=self.password) as conn:
            with conn.cursor() as cur:
                query = 'SELECT * FROM public.users'
                self.logger.write_lo_log(f'Read from DB -- {query}', 'INFO')
                cur.execute(query)
                users=[]
                for record in cur:
                    users.append({'id':record[0],'full_name':record[1],'password':record[2],'real_id':record[3]})
        return users

    def getUser(self,real_id):
        with psycopg2.connect(host=self.server, database=self.database, user=self.user,
                              password=self.password) as conn:
            with conn.cursor() as cur:
                query = 'SELECT * FROM public.users WHERE real_id=%s'
                value = (real_id,)
                self.logger.write_lo_log(f'Read from DB -- {query},{value}', 'INFO')
                cur.execute(query,value)
                if cur.rowcount==0:
                    return {}
                result=cur.fetchone()
        return {'id':result[0],'full_name':result[1],'password':result[2],'real_id':result[3]}

    def addUser(self,values_dict):
        try:
            with psycopg2.connect(host=self.server,database=self.database,user=self.user,
                                  password=self.password) as conn:
                with conn.cursor() as cur:
                    query='INSERT INTO public.users(full_name, password, real_id) VALUES(%s,%s,%s) RETURNING id'
                    values=(values_dict['name'],values_dict['password'],values_dict['real_id'])
                    self.logger.write_lo_log(f'Write to DB -- {query},{values}', 'INFO')
                    cur.execute(query,values)
                    result=cur.fetchone()
                conn.commit()
            return {'id':result[0]}
        except psycopg2.errors.UniqueViolation:
            err_msg='Error adding user, such ID already exists'
            self.logger.write_lo_log(err_msg, 'ERROR')
            raise DbError(err_msg)

    def updateUser(self, real_id,credentials):
        with psycopg2.connect(host=self.server, database=self.database, user=self.user,
                              password=self.password) as conn:
            with conn.cursor() as cur:
                query = 'UPDATE public.users SET full_name=%s, password=%s WHERE real_id=%s'
                values = (credentials['name'], credentials['password'], real_id)
                self.logger.write_lo_log(f'Write to DB -- {query},{values}', 'INFO')
                cur.execute(query, values)
                result = cur.rowcount #number of affected rows
            conn.commit()
        return {'affected':result}

    def deleteUser(self,real_id):
        with psycopg2.connect(host=self.server, database=self.database, user=self.user,
                              password=self.password) as conn:
            with conn.cursor() as cur:
                query = 'DELETE FROM public.users WHERE real_id=%s'
                value = (real_id,)
                self.logger.write_lo_log(f'Write to DB -- {query},{value}', 'INFO')
                cur.execute(query, value)
                result = cur.rowcount  # number of affected rows
            conn.commit()
        return {'affected':result}

    def getCountries(self):
        with psycopg2.connect(host=self.server, database=self.database, user=self.user,
                              password=self.password) as conn:
            with conn.cursor() as cur:
                query = 'SELECT * FROM public.countries'
                self.logger.write_lo_log(f'Read from DB -- {query}', 'INFO')
                cur.execute(query)
                countries = []
                for record in cur:
                    countries.append({'code': record[0], 'name': record[1]})
        return countries

    def getCountry(self,code):
        with psycopg2.connect(host=self.server, database=self.database, user=self.user,
                              password=self.password) as conn:
            with conn.cursor() as cur:
                query = 'SELECT * FROM public.countries WHERE code=%s'
                value = (code,)
                self.logger.write_lo_log(f'Read from DB -- {query},{value}', 'INFO')
                cur.execute(query,value)
                result=cur.fetchone()
        return {'code': result[0], 'name': result[1]}

    def addCountry(self,values_dict):
        try:
            with psycopg2.connect(host=self.server,database=self.database,user=self.user,
                                  password=self.password) as conn:
                with conn.cursor() as cur:
                    query='INSERT INTO public.countries(name) VALUES(%s) RETURNING code'
                    values=(values_dict['name'],)
                    self.logger.write_lo_log(f'Write to DB -- {query},{values}', 'INFO')
                    cur.execute(query,values)
                    result=cur.fetchone()
                conn.commit()
            return {'code':result[0]}
        except psycopg2.errors.UniqueViolation:
            err_msg='Error inserting to DB due to Unique constraint'
            self.logger.write_lo_log(err_msg, 'ERROR')
            raise DbError(err_msg)

    def deleteCountry(self,code):
        try:
            with psycopg2.connect(host=self.server, database=self.database, user=self.user,
                                  password=self.password) as conn:
                with conn.cursor() as cur:
                    query = 'DELETE FROM public.countries WHERE code=%s'
                    value = (code,)
                    self.logger.write_lo_log(f'Write to DB -- {query},{value}', 'INFO')
                    cur.execute(query, value)
                    result = cur.rowcount  # number of affected rows
                conn.commit()
            return {'affected':result}
        except psycopg2.errors.ForeignKeyViolation:
            err_msg='Error deleting from DB due to relation with existing record'
            self.logger.write_lo_log(err_msg, 'ERROR')
            raise DbError(err_msg)

    def getFlights(self):
        with psycopg2.connect(host=self.server, database=self.database, user=self.user,
                              password=self.password) as conn:
            with conn.cursor() as cur:
                query = 'SELECT * FROM public.flights'
                self.logger.write_lo_log(f'Read from DB -- {query}', 'INFO')
                cur.execute(query)
                flights = []
                for record in cur:
                    flights.append({'flight_id': record[0], 'timestamp': str(record[1]), 'remaining_seats': record[2], 'origin_country_id': record[3], 'dest_country_id': record[4]})
        return flights

    def getFlight(self,flight_id):
        with psycopg2.connect(host=self.server, database=self.database, user=self.user,
                              password=self.password) as conn:
            with conn.cursor() as cur:
                query = 'SELECT * FROM public.flights WHERE flight_id=%s'
                value = (flight_id,)
                self.logger.write_lo_log(f'Read from DB -- {query},{value}', 'INFO')
                cur.execute(query,value)
                result=cur.fetchone()
        return {'flight_id': result[0], 'timestamp': str(result[1]), 'remaining_seats': result[2], 'origin_country_id': result[3], 'dest_country_id': result[4]}

    def addFlight(self,values_dict):
        with psycopg2.connect(host=self.server,database=self.database,user=self.user,
                              password=self.password) as conn:
            with conn.cursor() as cur:
                query='INSERT INTO public.flights(timestamp,remaining_seats,origin_country_id,dest_country_id) VALUES(%s,%s,%s,%s) RETURNING flight_id'
                values=(values_dict['timestamp'],values_dict['remaining_seats'],values_dict['origin_country_id'],values_dict['dest_country_id'])
                self.logger.write_lo_log(f'Write to DB -- {query},{values}', 'INFO')
                cur.execute(query,values)
                result=cur.fetchone()
            conn.commit()
        return {'flight_id':result[0]}

    def updateFlight(self, flight_id, flight_dict):
        with psycopg2.connect(host=self.server, database=self.database, user=self.user,
                              password=self.password) as conn:
            with conn.cursor() as cur:
                query = 'UPDATE public.flights SET remaining_seats=%s WHERE flight_id=%s'
                values = (flight_dict['remaining_seats'], flight_id)
                self.logger.write_lo_log(f'Write to DB -- {query},{values}', 'INFO')
                cur.execute(query, values)
                result = cur.rowcount #number of affected rows
            conn.commit()
        return {'affected':result}

    def deleteFlight(self,flight_id):
        with psycopg2.connect(host=self.server, database=self.database, user=self.user,
                              password=self.password) as conn:
            with conn.cursor() as cur:
                query = 'DELETE FROM public.flights WHERE flight_id=%s'
                value = (flight_id,)
                self.logger.write_lo_log(f'Write to DB -- {query},{value}', 'INFO')
                cur.execute(query, value)
                result = cur.rowcount  # number of affected rows
            conn.commit()
        return {'affected':result}

    def getTickets(self):
        with psycopg2.connect(host=self.server, database=self.database, user=self.user,
                              password=self.password) as conn:
            with conn.cursor() as cur:
                query = 'SELECT * FROM public.tickets'
                self.logger.write_lo_log(f'Read from DB -- {query}', 'INFO')
                cur.execute(query)
                tickets = []
                for record in cur:
                    tickets.append({'ticket_id': record[0], 'user_id': record[1], 'flight_id': record[2]})
        return tickets

    def getTicket(self,ticket_id):
        with psycopg2.connect(host=self.server, database=self.database, user=self.user,
                              password=self.password) as conn:
            with conn.cursor() as cur:
                query = 'SELECT * FROM public.tickets WHERE ticket_id=%s'
                value = (ticket_id,)
                self.logger.write_lo_log(f'Read from DB -- {query},{value}', 'INFO')
                cur.execute(query,value)
                if cur.rowcount==0:
                    return {}
                result=cur.fetchone()
        return {'ticket_id': result[0], 'user_id': result[1], 'flight_id': result[2]}

    def addTicket(self,values_dict):
        with psycopg2.connect(host=self.server,database=self.database,user=self.user,
                              password=self.password) as conn:
            with conn.cursor() as cur:
                query='INSERT INTO public.tickets(user_id,flight_id) VALUES(%s,%s) RETURNING ticket_id'
                values=(values_dict['user_id'],values_dict['flight_id'])
                self.logger.write_lo_log(f'Write to DB -- {query},{values}', 'INFO')
                cur.execute(query,values)
                result=cur.fetchone()
            conn.commit()
        return {'ticket_id':result[0]}

    def deleteTicket(self,ticket_id):
        with psycopg2.connect(host=self.server, database=self.database, user=self.user,
                              password=self.password) as conn:
            with conn.cursor() as cur:
                query = 'DELETE FROM public.tickets WHERE ticket_id=%s'
                value = (ticket_id,)
                self.logger.write_lo_log(f'Write to DB -- {query},{value}', 'INFO')
                cur.execute(query, value)
                result = cur.rowcount  # number of affected rows
            conn.commit()
        return {'affected':result}