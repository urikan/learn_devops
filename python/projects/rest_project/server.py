from rest_project.db_api import PgSqlConnection,DbError
from flask import Flask,request,Response
import rest_project.logging_api as logger
import json,datetime

def init_logger():
    with open('server_conf.json') as json_file:
        conf = json.load(json_file)
        logger.init(f'{conf["log"]["file_location"]}'+
                    f'{datetime.datetime.now().year}_'+
                    f'{datetime.datetime.now().month}_' +
                    f'{datetime.datetime.now().day}_' +
                    f'{datetime.datetime.now().hour}_' +
                    f'{datetime.datetime.now().minute}_' +
                    f'{datetime.datetime.now().second}' + '.log'
                    , conf["log"]["level"])

init_logger()
logger.write_lo_log('**************** Server started ...', 'INFO')
db_conn=PgSqlConnection(logger)

app = Flask(__name__)

@app.route('/users', methods = ['GET'])
def getUsers():
    return Response(json.dumps(db_conn.getUsers()), status=200, mimetype='application/json')

@app.route('/users/<string:id>', methods = ['GET'])
def getUser(id):
    user_details=db_conn.getUser(id)
    if len(user_details)==0:
        return Response('{"message":"Such user was not found"}', status=202, mimetype='application/json')
    return Response(json.dumps(user_details), status=200, mimetype='application/json')

@app.route('/users', methods = ['POST'])
def addUser():
    try:
        res=db_conn.addUser(request.get_json())
        return Response(json.dumps(res), status=200, mimetype='application/json')
    except DbError as error:
        return Response(json.dumps({"message":error.msg}), status=202, mimetype='application/json')

@app.route('/users/<string:id>', methods = ['PUT'])
def updateUser(id):
    res=db_conn.updateUser(id,request.get_json())
    return Response(json.dumps(res), status=200, mimetype='application/json')

@app.route('/users/<string:id>', methods = ['DELETE'])
def deleteUser(id):
    return Response(json.dumps(db_conn.deleteUser(id)), status=200, mimetype='application/json')

@app.route('/countries', methods = ['GET'])
def getCountries():
    return Response(json.dumps(db_conn.getCountries()), status=200, mimetype='application/json')

@app.route('/countries/<string:code>', methods = ['GET'])
def getCountry(code):
    return Response(json.dumps(db_conn.getCountry(code)), status=200, mimetype='application/json')

@app.route('/countries', methods = ['POST'])
def addCountry():
    res=db_conn.addCountry(request.get_json())
    return Response(json.dumps(res), status=200, mimetype='application/json')

@app.route('/countries/<string:code>', methods = ['DELETE'])
def deleteCountry(code):
    return Response(json.dumps(db_conn.deleteCountry(code)), status=200, mimetype='application/json')

@app.route('/flights', methods = ['GET'])
def getFlights():
    return Response(json.dumps(db_conn.getFlights()), status=200, mimetype='application/json')

@app.route('/flights/<string:id>', methods = ['GET'])
def getFlight(id):
    return Response(json.dumps(db_conn.getFlight(id)), status=200, mimetype='application/json')

@app.route('/flights', methods = ['POST'])
def addFlight():
    res=db_conn.addFlight(request.get_json())
    return Response(json.dumps(res), status=200, mimetype='application/json')

@app.route('/flights/<string:id>', methods = ['PUT'])
def updateFlight(id):
    res=db_conn.updateFlight(id,request.get_json())
    return Response(json.dumps(res), status=200, mimetype='application/json')

@app.route('/flights/<string:id>', methods = ['DELETE'])
def deleteFlight(id):
    return Response(json.dumps(db_conn.deleteFlight(id)), status=200, mimetype='application/json')

@app.route('/tickets', methods = ['GET'])
def getTickets():
    return Response(json.dumps(db_conn.getTickets()), status=200, mimetype='application/json')

@app.route('/tickets/<string:id>', methods = ['GET'])
def getTicket(id):
    return Response(json.dumps(db_conn.getTicket(id)), status=200, mimetype='application/json')

@app.route('/tickets', methods = ['POST'])
def addTicket():
    flight_id=request.get_json()['flight_id']
    remaining_seats=db_conn.getFlight(flight_id)['remaining_seats']
    if remaining_seats==0:
        return Response('{"message":"No seats remained on the flight"}', status=202, mimetype='application/json')
    db_conn.updateFlight(flight_id,{'remaining_seats':remaining_seats-1})
    add_res=db_conn.addTicket(request.get_json())
    return Response(json.dumps(add_res), status=200, mimetype='application/json')

@app.route('/tickets/<string:id>', methods = ['DELETE'])
def deleteTicket(id):
    ticket_details=db_conn.getTicket(id)
    if len(ticket_details)==0:
        return Response('{"message":"Such ticket was not found"}', status=202, mimetype='application/json')
    flight_id=ticket_details['flight_id']
    remaining_seats = db_conn.getFlight(flight_id)['remaining_seats']
    db_conn.updateFlight(flight_id, {'remaining_seats': remaining_seats + 1})
    delete_res=db_conn.deleteTicket(id)
    if delete_res['affected']==1:
        return Response('{"message":"Successfully deleted ticket"}', status=200, mimetype='application/json')
    else:
        return Response('{"message":"Error occured while deleting ticket"}', status=202, mimetype='application/json')

app.run()