import json,datetime,tkinter as tk
from tkinter import ttk
from rest_project.client_api import FlightTicketsRestClient
import rest_project.logging_api as logger

tickets=[]
flights=[]
directions=[]
row_elements=[]
window = tk.Tk()
window.title("Welcome to Postwings")
row_select=tk.IntVar(master=window)
flight_select=tk.StringVar(master=window)

def init_logger():
    with open('client_conf.json') as json_file:
        conf = json.load(json_file)
        logger.init(f'{conf["log"]["file_location"]}' +
                    f'{datetime.datetime.now().year}_' +
                    f'{datetime.datetime.now().month}_' +
                    f'{datetime.datetime.now().day}_' +
                    f'{datetime.datetime.now().hour}_' +
                    f'{datetime.datetime.now().minute}_' +
                    f'{datetime.datetime.now().second}' + '.log'
                    , conf["log"]["level"])

#'user register' command handler
def register():
    logger.write_lo_log(f'Register new user - name: {ent_newName.get()}, password: {ent_newPass.get()}, ID: {ent_newId.get()}', 'INFO')
    res=rest_api.registerUser(ent_newName.get(), ent_newPass.get(), ent_newId.get())
    lbl_registerMessage['text']=res[1]
    if res[0]:
        ent_newName.delete(0, tk.END)
        ent_newPass.delete(0, tk.END)
        ent_newId.delete(0, tk.END)
        lbl_registerMessage['foreground']= 'black'
    else:
        lbl_registerMessage['foreground'] = 'red'

#'user login' command handler
def login(id,password):
    logger.write_lo_log(f'User login - ID: {id}, password: {password}', 'INFO')
    res = rest_api.loginUser(id,password)
    if res[0]:
        ent_userId.delete(0, tk.END)
        ent_userPass.delete(0, tk.END)
        lbl_greet['text']=f"Hello, {res[2]}"
        frm_register.pack_forget()
        frm_login.pack_forget()
        lbl_registerMessage['text']=''
        lbl_loginMessage['text']=''
        lbl_loginMessage['foreground'] = 'black'
        btn_logout.grid()
        frm_tickets.pack(anchor="w")
        frm_deleteTickets.pack(anchor="w")
        frm_buyTickets.pack(anchor="w")
        listTickets()
    else:
        lbl_loginMessage['text'] = res[1]
        lbl_loginMessage['foreground'] = 'red'

#'user logout' command handler
def logout():
    logger.write_lo_log('User logout', 'INFO')
    frm_register.pack(anchor="w")
    frm_login.pack(anchor="w")
    frm_tickets.pack_forget()
    frm_deleteTickets.pack_forget()
    frm_buyTickets.pack_forget()
    lbl_deleteMessage['text']=''
    lbl_buyMessage['text']=''
    lbl_greet['text'] = f"Hello, guest"
    btn_logout.grid_remove()

#refresh tickets and flights
def listTickets():
    global tickets
    tickets=rest_api.getTickets()
    global flights
    flights=rest_api.getFlights()
    global directions
    directions.clear()
    for flight in flights:
        countries = rest_api.getDirection(flight)
        directions.append(f'{countries[0]} --> {countries[1]}')
    cmb_flights['values']=directions
    flight_select.set('')
    btn_buyTicket['state']=tk.DISABLED
    for element in row_elements:
        element.destroy()
    row_elements.clear()
    row_count=1
    for ticket in tickets:
        lbl_ticketId = tk.Label(master=frm_tickets, text=ticket['ticket_id'])
        lbl_ticketId.grid(row=row_count, column=0)
        row_elements.append(lbl_ticketId)

        lbl_flightId = tk.Label(master=frm_tickets, text=ticket['flight_id'])
        lbl_flightId.grid(row=row_count, column=1)
        row_elements.append(lbl_flightId)

        lbl_direction=tk.Label(master=frm_tickets, text=getDirection(ticket['flight_id']))
        lbl_direction.grid(row=row_count, column=2)
        row_elements.append(lbl_direction)

        rbt_ticketSelect=tk.Radiobutton(master=frm_tickets,variable=row_select,value=row_count-1,command=ticketSelectedHandler)
        rbt_ticketSelect.grid(row=row_count, column=3)
        row_elements.append(rbt_ticketSelect)

        row_count=row_count+1
    row_select.set(-1)
    btn_deleteTicket['state']=tk.DISABLED

#get a flight from-to direction
def getDirection(flightId):
    index=0
    for flight in flights:
        if flight['flight_id']==flightId:
            return directions[index]
        index=index+1

#'ticket delete' command handler
def deleteTicket():
    lbl_buyMessage['text']=''
    ticket=tickets.pop(row_select.get())
    logger.write_lo_log(f'Delete ticket - ID: {ticket["ticket_id"]}', 'INFO')
    res = rest_api.deleteTicket(ticket['ticket_id'])
    lbl_deleteMessage['text']=res[1]
    if res[0]:
        lbl_deleteMessage['foreground'] = 'black'
        listTickets()
    else:
        lbl_deleteMessage['foreground'] = 'red'

def ticketSelectedHandler():
    btn_deleteTicket['state'] = tk.ACTIVE

#'buy ticket' command handler
def buyTicket():
    lbl_deleteMessage['text'] = ''
    flight=getFlight()
    logger.write_lo_log(f'Buy ticket - flight ID: {flight["flight_id"]}', 'INFO')
    res=rest_api.buyTicket(flight['flight_id'])
    lbl_buyMessage['text']=res[1]
    if res[0]:
        lbl_buyMessage['foreground'] = 'black'
        listTickets()
    else:
        lbl_buyMessage['foreground'] = 'red'

#get selected flight
def getFlight():
    index=0
    for direction in directions:
        if direction==flight_select.get():
            return flights[index]
        index=index+1

def flightSelectedHandler(event):
    btn_buyTicket['state']=tk.ACTIVE

with open('client_conf.json') as json_file:
    conf = json.load(json_file)
    serverUrl=conf['server_url']
rest_api=FlightTicketsRestClient(serverUrl)
init_logger()
logger.write_lo_log('**************** Client started ...', 'INFO')

frm_greet = tk.Frame(master=window)
frm_greet.pack()
frm_greet.rowconfigure(0, minsize=30, weight=1)
frm_greet.columnconfigure([0,1], minsize=30, weight=1)

lbl_greet = tk.Label(master=frm_greet, text="Hello, guest")
lbl_greet.grid(row=0, column=0)

btn_logout = tk.Button(master=frm_greet, text="Logout",command=logout)
btn_logout.grid(row=0, column=1)
btn_logout.grid_remove()

frm_register = tk.Frame(master=window)
frm_register.pack(anchor="w")
frm_register.rowconfigure(0, minsize=30, weight=1)
frm_register.columnconfigure([0, 1, 2, 3, 4, 5, 6, 7, 8], minsize=30, weight=1)

lbl_newUser = tk.Label(master=frm_register, text="New user   -- ")
lbl_newUser.grid(row=0, column=0)

lbl_newName = tk.Label(master=frm_register, text="Full name:")
lbl_newName.grid(row=0, column=1)

ent_newName = tk.Entry(master=frm_register, text="")
ent_newName.grid(row=0, column=2)

lbl_newPass = tk.Label(master=frm_register, text="Password:")
lbl_newPass.grid(row=0, column=3)

ent_newPass = tk.Entry(master=frm_register, text="",show='*')
ent_newPass.grid(row=0, column=4)

lbl_newId = tk.Label(master=frm_register, text="ID:")
lbl_newId.grid(row=0, column=5)

ent_newId = tk.Entry(master=frm_register, text="")
ent_newId.grid(row=0, column=6)

btn_register = tk.Button(master=frm_register, text="Register", command=register)
btn_register.grid(row=0, column=7)

lbl_registerMessage = tk.Label(master=frm_register, text="")
lbl_registerMessage.grid(row=0, column=8)

frm_login = tk.Frame(master=window)
frm_login.pack(anchor="w")
frm_login.rowconfigure(0, minsize=30, weight=1)
frm_login.columnconfigure([0, 1, 2, 3, 4, 5, 6], minsize=30, weight=1)

lbl_user = tk.Label(master=frm_login, text="Registered user   -- ")
lbl_user.grid(row=0, column=0)

lbl_userId = tk.Label(master=frm_login, text="ID:")
lbl_userId.grid(row=0, column=1)

ent_userId = tk.Entry(master=frm_login, text="")
ent_userId.grid(row=0, column=2)

lbl_userPass = tk.Label(master=frm_login, text="Password:")
lbl_userPass.grid(row=0, column=3)

ent_userPass = tk.Entry(master=frm_login, text="",show='*')
ent_userPass.grid(row=0, column=4)

btn_login = tk.Button(master=frm_login, text="Login", command=lambda: login(ent_userId.get(), ent_userPass.get()))
btn_login.grid(row=0, column=5)

lbl_loginMessage = tk.Label(master=frm_login, text="")
lbl_loginMessage.grid(row=0, column=6)

frm_tickets = tk.Frame(master=window)

lbl_ticketsId = tk.Label(master=frm_tickets, text="Ticket ID")
lbl_ticketsId.grid(row=0,column=0)

lbl_flightsId = tk.Label(master=frm_tickets,text="Flight ID")
lbl_flightsId.grid(row=0,column=1)

lbl_flightsDirection = tk.Label(master=frm_tickets,text="Direction")
lbl_flightsDirection.grid(row=0,column=2)

frm_deleteTickets = tk.Frame(master=window)

btn_deleteTicket = tk.Button(master=frm_deleteTickets, text="Delete ticket", command=deleteTicket)
btn_deleteTicket.grid(row=0, column=0)

lbl_deleteMessage = tk.Label(master=frm_deleteTickets,text='')
lbl_deleteMessage.grid(row=0,column=1)

frm_buyTickets = tk.Frame(master=window)

cmb_flights=ttk.Combobox(master=frm_buyTickets,textvariable=flight_select)
cmb_flights.state(["readonly"])
cmb_flights.grid(row=0, column=0)
cmb_flights.bind("<<ComboboxSelected>>", flightSelectedHandler)

btn_buyTicket = tk.Button(master=frm_buyTickets, text="Buy ticket", command=buyTicket)
btn_buyTicket.grid(row=0, column=1)

lbl_buyMessage = tk.Label(master=frm_buyTickets,text='')
lbl_buyMessage.grid(row=0,column=2)

window.mainloop()