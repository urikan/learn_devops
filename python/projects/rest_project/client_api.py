import requests,json

class FlightTicketsRestClient:
    def __init__(self,serverUrl):
        self.serverUrl=serverUrl
        self.userId=-1

    def registerUser(self, fullname,password,realId):
        if fullname=='' or password=='' or realId=='':
            return False,'Missing some parameters'
        resp=requests.post(f'{self.serverUrl}/users',data=json.dumps({"name": fullname,"password": password,"real_id": realId}),headers={'Content-Type':'application/json'})
        if resp.status_code==200:
            return True,'User added'
        if resp.status_code == 202:
            return False,json.loads(resp.content)['message']

    def loginUser(self, realId, password):
        if realId=='' or password=='':
            return False,'Missing some parameters'
        resp=requests.get(f'{self.serverUrl}/users/{realId}')
        response_data=json.loads(resp.content)
        if resp.status_code == 202:
            return False,response_data['message']
        if resp.status_code==200:
            if response_data['password']==password:
                self.userId=response_data['id']
                return True,'Login successful',response_data['full_name']
            else:
                return False,'Wrong password'

    def getTickets(self):
        resp=requests.get(f'{self.serverUrl}/tickets')
        response_data = json.loads(resp.content)
        user_tickets=[]
        for ticket in response_data:
            if ticket['user_id']==self.userId:
                ticket.pop('user_id')
                user_tickets.append(ticket)
        return user_tickets

    def deleteTicket(self,ticketId):
        resp=requests.delete(f'{self.serverUrl}/tickets/{ticketId}')
        response_data = json.loads(resp.content)
        if resp.status_code == 202:
            return False,response_data['message']
        if resp.status_code == 200:
            return True,response_data['message']

    def getFlights(self):
        resp=requests.get(f'{self.serverUrl}/flights')
        return json.loads(resp.content)

    def getDirection(self, flight):
        resp=requests.get(f'{self.serverUrl}/countries/{flight["origin_country_id"]}')
        originCountry=json.loads(resp.content)['name']
        resp = requests.get(f'{self.serverUrl}/countries/{flight["dest_country_id"]}')
        destinationCountry=json.loads(resp.content)['name']
        return originCountry,destinationCountry

    def buyTicket(self,flightId):
        resp=requests.post(f'{self.serverUrl}/tickets',data=json.dumps({"user_id": self.userId,"flight_id": int(flightId)}),headers={'Content-Type':'application/json'})
        if resp.status_code==200:
            return True,'Purchase successful'
        if resp.status_code == 202:
            return False,json.loads(resp.content)['message']