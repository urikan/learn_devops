#-------Question 8 solution-------
def calculateCircle(radius):
    return 3.14*radius**2
            
#-------Question 9 solution-------
def add(x=0,y=0):
    return x+y
    
def sub(x=0,y=0):
    return x-y
    
def mul(x=0,y=0):
    return x*y
    
def div(x=0,y=0):
    return x/y
    
num1=int(input('first number: '))
num2=int(input('second number: '))
print(f'+: {add(num1,num2)} ,-: {sub(num1,num2)} ,*: {mul(num1,num2)} ,/: {div(num1,num2)}')

#-------Question 10 solution-------
def getInRange(min,max):
    while True:
        num=int(input('insert number: '))
        if num in range(min,max+1):
            return num

#-------Question 11 solution-------
def lowestNumber(num1,num2,num3):
    return min({num1,num2,num3})