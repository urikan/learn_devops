#-------Question 3 solution-------
list=[1,5,7,8,100]
for num in list[:len(list)//2]:
    print()#do something

#-------Question 4 solution-------
names=['Hagay','Doron','Anna','Marina']
for name in names:
    print(name.upper())

#-------Question 5 solution-------
names=['Hagay','Ben','Anna','Marina']
for name in names:
    if len(name)<4:
        break;
    print(name.upper())

# -------Question 6 solution-------
full_name = 'Uri Gorodetsky'
for letter in full_name[-5:]:
    print(letter)
for letter in full_name[:len(full_name) // 3]:
    print(letter)
print(full_name.count('a'))
print('b' in full_name)
name_fields=full_name.split()
name_fields.reverse()
name_fields[0]=name_fields[0].upper()
first_name=name_fields[1]
half_length=len(first_name)//2
name_fields[1]=first_name[:half_length]+first_name[half_length+1:]
print('{0} {1}'.format(name_fields[0],name_fields[1]))

# -------Question 7 solution-------
hello_str='Hello world'
first_index=hello_str.index('o')
last_index=hello_str.rindex('o')
print(hello_str[:first_index])
print(hello_str[last_index:])

#-------Question 8 solution-------
hello_str='Hello world'
print(hello_str.replace('o',''))

# -------Question 9 solution-------
num_list=[8, 1000, -3, 2, 5]
list_sum=sum(num_list)
print(f'Sum is {sum}')
print(max(num_list))
print(min(num_list))
avg=list_sum/len(num_list)
print(avg)
num_list.pop(len(num_list)//2)
num_list.sort()
for it in range(5):
    print(num_list)
num_list.pop(0)
sub_list=[]
for num in num_list:
    if num<avg:
        sub_list.append(num)

# -------Question 10 solution-------
num_list=[1,5,7,8,100]
big=0
for number in num_list:
    if number>big or first:
        big=number
        first=False
print(big)

# -------Question 11 solution-------
lists=[[4,8,200],[4,3000,-1],[5,87,12]]
small=0
first=True
for list in lists:
    for num in list:
        if num<small or first:
            small=num
            first=False
print(small)