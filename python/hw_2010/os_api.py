import os

def printFilesUniqueCount(path):
    files_set = set()
    for where_am_i, list_of_folders, list_of_files in os.walk(path):
        for file in list_of_files:
            files_set.add(file)
    print(f'Unique files under given tree: {len(files_set)}')

if __name__ == '__main__':
    printFilesUniqueCount(r'C:\Users\UriG\Documents\education\dir1')