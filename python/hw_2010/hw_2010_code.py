# -------Task 1 solution-------
import os_api

os_api.printFilesUniqueCount(r'C:\Users\UriG\Documents\education\dir1')

# -------Task 2 solution-------
import psutil
import os

app_paths=[r'C:\Program Files\Notepad++\notepad++.exe',\
    r'C:\Program Files (x86)\Microsoft\Edge\Application\msedge.exe',\
    r'C:\Program Files\Google\Chrome\Application\chrome.exe',\
    r'C:\Program Files\Internet Explorer\iexplore.exe',\
    r'C:\Program Files (x86)\Microsoft\Skype for Desktop\Skype.exe']
app_names=['notepad++.exe','msedge.exe','chrome.exe','iexplore.exe','Skype.exe']

while True:
    print('1 - Notepad++\n2 - Edge\n3 - Chrome\n4 - Internet Explorer\n5 - Skype')
    try:
        choice=int(input('Please choose a program to start: '))-1
    except ValueError:
        print('Illegal input, please try again')
        continue
    if choice not in range(len(app_names)):
        print('not on list')
        continue
    proc_running=False
    for proc in psutil.process_iter():
        try:
            if proc.name()==app_names[choice]:
                print('already running')
                proc_running=True
                break
        except:
            print('error')
    if not proc_running:
        os.startfile(app_paths[choice])
        break
        
# -------Task 3 solution-------
import math

def getDirectorySize(path):
    size=0
    for where_am_i, list_of_folders, list_of_files in os.walk(path):
        for file_name in list_of_files:
            file_path=os.path.join(where_am_i, file_name)
            try:
                size = size + os.path.getsize(file_path)
            except OSError:
                pass
    return size

root_path='/'
dir_sizes={}
total_size=0
with os.scandir(root_path) as it:
    for entry in it:
        if entry.is_dir(follow_symlinks=False):
            dir_size= getDirectorySize(entry.path)
            dir_sizes[entry.name]=dir_size
            total_size=total_size+dir_size
for dir_name,dir_size in dir_sizes.items():
    print(f'{dir_name} {dir_size} {math.trunc(dir_size/total_size*100)}%')