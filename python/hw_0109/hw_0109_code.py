# -------Question 1 solution-------
def sort_tuple(*args):
    vars_list = []
    for arg in args:
        vars_list.append(arg)
    vars_list.sort()
    return tuple(vars_list)


# -------Question 2 solution-------
def dict_compare(ref_dict, **kwargs):
    for key, value in ref_dict.items():
        if key in kwargs.keys() and kwargs[key] == value:
            kwargs.pop(key)
        else:
            return False
    return len(kwargs) == 0


# -------Question 3 solution-------
class SuperHero:
    def __init__(self):
        print('new super hero is born')

    def forceSpeed(self):
        print('I am fast')

    def climb(self):
        print('I am climbing')

    def fly(self):
        print('I am flying')


superman = SuperHero()
batman = SuperHero()
wonderwoman = SuperHero()
superman.forceSpeed()
superman.climb()
superman.fly()
batman.forceSpeed()
batman.climb()
batman.fly()
wonderwoman.forceSpeed()
wonderwoman.climb()
wonderwoman.fly()
