import requests
import json

# -------Question 11 solution-------
class Photo:
    def __init__(self,data_dict):
        self.__dict__=data_dict
    def __str__(self):
        out_str='Photo:'
        for k,v in self.__dict__.items():
            out_str=out_str+f' {k}={v}'
        return out_str

baseUrl='https://jsonplaceholder.typicode.com/photos/'
id=input('please provide id for required photo: ')
resp = requests.get(f'{baseUrl}{id}')
respose_code=resp.status_code
if respose_code==200:
    print(Photo(json.loads(resp.content)))
else:
    print('photo does not exist')

# -------Question 12 solution-------
class Company:
    def __init__(self,data_dict):
        self.__dict__=data_dict
    def __str__(self):
        out_str='Company:'
        for k,v in self.__dict__.items():
            out_str=out_str+f' {k}={v}'
        return out_str

class Geo:
    def __init__(self,data_dict):
        self.__dict__=data_dict
    def __str__(self):
        out_str='Geo:'
        for k,v in self.__dict__.items():
            out_str=out_str+f' {k}={v}'
        return out_str

class Address:
    def __init__(self,data_dict):
        self.__dict__=data_dict
        self.geo=Geo(data_dict['geo'])
    def __str__(self):
        out_str='Address:'
        for k,v in self.__dict__.items():
            out_str=out_str+f' {k}={v}'
        return out_str

class User:
    def __init__(self,data_dict):
        self.__dict__=data_dict
        self.address=Address(data_dict['address'])
        self.company=Company(data_dict['company'])
    def __str__(self):
        out_str='User:'
        for k,v in self.__dict__.items():
            out_str=out_str+f' {k}={v}'
        return out_str

name=input('please provide name of user: ')
resp = requests.get('https://jsonplaceholder.typicode.com/users')
for user_data in json.loads(resp.content):
    user=User(user_data)
    print(user)
    if user.name==name:
        break
