#-------Question 11 solution-------
sen="I love to eat ice cream in the beach"
upper_list=[word.upper() for word in sen.split()]
first_list=[word[0] for word in sen.split()]
third_list=[word[2] for word in sen.split() if len(word)>2]
len_list=[len(word) for word in sen.split()]

#-------Question 12 solution-------
power_list=[10**power for power in range(1,10)]

#-------Question 13 solution-------
def tryGetValue(dict,key):
    if key in dict.keys():
        return dict[key]
    return None
    
#-------Question 14 solution-------
def getSortedKeys(dict):
    dict_keys=list(dict.keys())
    dict_keys.sort()
    return dict_keys

#-------Question 15 solution-------
def mergeDictionary(dict0,dict1):
    merge_dict=dict0.copy()
    for key,value in dict1.items():
        merge_dict[key]=value
    return merge_dict

#-------Question 16 solution-------
dict={}
while True:
    key=input('enter your id: ')
    if key=='-1':
        break
    if key in dict.keys():
        print(f'key {key} already exists in dictionary')
        continue
    value=input('enter your name: ')
    dict[key]=value
print(f'dictionary content is: {dict}')