# -------Question 13 solution-------
numbers_dict={}
while True:
    user_inp=input('please enter a number (-1 to end): ')
    if user_inp=='-1':
        break
    if not user_inp.isdigit():
        print('illegal input')
        continue
    number=int(user_inp)
    if number in numbers_dict.keys():
        numbers_dict[number]=numbers_dict[number]+1
        continue
    numbers_dict[number]=1
print(f'numbers inserted: {sum(numbers_dict.values())}')
print(f'uniq numbers: {len(numbers_dict)}')
duplicates=0
for number in numbers_dict:
    if numbers_dict[number]>1:
        duplicates=duplicates+1
print(f'duplicated numbers: {duplicates}')

# -------Question 14 solution-------
words=[]
while True:
    user_inp=input('please enter a word (\'exit\' to end): ')
    if user_inp=='exit':
        break
    words.append(user_inp)
with open('word.txt','w') as file:
    for word in words:
        file(f'{word} ')
with open('word.txt','r') as file:
    print(file.read())
    
# -------Question 15 solution-------
def WriteSum(path):
    sum=0
    with open(path,'r') as file:
        for line in file:
            sum=sum+int(line)
    with open(path,'a') as file:
        file.write(f'{sum}\n')
        
# -------Question 16 solution-------
def FindWord(path,user_word):
    words=set()
    with open(path, 'r') as file:
        for line in file:
            for word in line.split():
                words.add(word)
    return user_word in words
    
# -------Question 17 solution-------
def WriteReverse(path_in,path_out):
    with open(path_in, 'r') as file_in:
        file_in.seek(0,2)
        file_size=file_in.tell()
        position=file_size
        block_size=10
        first_block=True
        with open(path_out, 'w') as file_out:
            while position!=0:
                new_position=position-block_size
                if new_position<0:
                    new_position=0
                file_in.seek(new_position)
                chars_to_read=position-new_position
                text=file_in.read(chars_to_read)
                if not first_block:
                    if '\n' in text:
                        text=text[:chars_to_read-1]
                else:
                    first_block=False
                file_out.write(text[::-1])
                position=new_position